<?php
class Usuario{  // Se crea una clase
    var $nombre='defecto'; // propiedad publica de una clase se le asigna defecto
    
    public function set_usuario(){ // metodo de una clase que pone la propiedad nombre con el valor Ramon
        $this->nombre='Ramon';
    }
}

$persona=new Usuario(); // se crea un objeto utilizando la clase usuario ese objeto toma los valores de esa clase.
echo $persona->nombre; // muestra la propiedad nombre del objeto persona 
$persona->nombre='silvia'; // cambia la propiedad nombre a silvia del objeto persona
var_dump($persona); // muestra las propiedades del objeto persona
$persona->set_usuario(); // ejecuta el procedimiento set_usuario del objeto persona
echo $persona->nombre; // muestra la propiedad nombre del objeto persona
