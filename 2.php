<?php

class Usuario {
    var $nombre = 'defecto';
    private $edad;
    protected $telefono;
    
    public function getNombre(){
        return $this->nombre;
    }
    
    public function getEdad(){
        return $this->edad;
    }
    
    public function getTelefono(){
        return $this->telefono;
    }
    
    public function setNombre($n){
        $this->nombre = $n;
    }
    
    public function setEdad($e){
        $this->edad = $e;
    }
    
    public function setTelefono($t){
        $this->telefono = $t;
    }
}

$persona = new Usuario();
echo $persona->nombre;
echo '</br>';

$persona->setEdad(51); // $persona->edad='51'  no se puede hacer si no es empleando el seter ya que es una propiedad privada.

echo $persona->getedad(); // $persona->edad no se puede mostrar al ser una propiedad privada hay que utilizar el metodo getEdad.
echo '</br>';

$persona ->setTelefono('232323');
echo $persona->getTelefono();
echo '</br>';
var_dump($persona);
$persona->nombre='Silvia';
var_dump($persona);
