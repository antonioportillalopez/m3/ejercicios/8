<?php
// antes de php5
/*function __autoload($nombre_clase) {
    include $nombre_clase . '.php';
}*/

// en php5

spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 




use clases\Coche;


$ibiza= new Coche();
$opel= new Coche();

$ibiza->color='negro';
$ibiza->numero_puertas='3';
$ibiza->modelo='stella';
echo $ibiza->acelerar();

$opel->color='gris';
$opel->numero_puertas='5';
$opel->modelo='meriva';

$opel->llenarTanque(40);
$opel->acelerar();
$opel->acelerar();
$opel->acelerar();

var_dump($opel);
var_dump($ibiza);

