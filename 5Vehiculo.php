<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Vehiculo;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $tractor= new Vehiculo('tractor','amarillo',false);
        var_dump($tractor);
        $tractor->encender();
        var_dump($tractor);
        $scooter= new Vehiculo();
        var_dump($scooter);
        ?>
    </body>
</html>
