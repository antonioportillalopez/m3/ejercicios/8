<?php
namespace clases;

class Vehiculo {
    
    public $matricula;
    private $color;
    protected $encendido;
    
    function __construct($su_matricula, $su_color, $si_funcionando) {
        $this->matricula=$su_matricula;
        $this->color=$su_color;
        $this->encendido=$si_funcionando;
    }
    
    public function encender(){
        $this->encendido=true;
        echo 'Vehiculo encendido<br>';
    }
    
    public function apagar(){
        $this->encendido=false;
        echo 'Vehiculo apagado<br>';
    }
    
}
