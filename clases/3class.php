<?php

class Usuario {
    var $nombre = 'defecto';
    private $edad;
    protected $telefono;
    private $apellidos="defecto";
    private $nombrecompleto;
    
    public function getNombreCompleto(){
        return $this->nombrecompleto;
    }
    
    public function getApellidos(){
        return $this->apellidos;
    }
    
    public function setApellidos($a){
        $this->apellidos = $a;
        $this->concatenar();
    }
    
    public function getNombre(){
        return $this->nombre;
    }
    
    public function getEdad(){
        return $this->edad;
    }
    
    public function getTelefono(){
        return $this->telefono;
    }
    
    public function setNombre($n){
        $this->nombre = $n;
        $this->concatenar();
    }
    
    public function setEdad($e){
        $this->edad = $e;
    }
    
    public function setTelefono($t){
        $this->telefono = $t;
    }
    
    private function concatenar(){
        $this->nombrecompleto=$this->nombre.' '.$this->apellidos;
    }
    
}

