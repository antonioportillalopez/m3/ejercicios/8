<?php
namespace clases;
class Coche{
    
    var $color;
    var $numero_puertas;
    var $modelo;
    var $gasolina = 0;
    
    function llenarTanque($gasolina_nueva){
        $this->gasolina=$this->gasolina+$gasolina_nueva;
    }
    
    function acelerar(){
        if ($this->gasolina>0){
            $this->gasolina=$this->gasolina-1;
            return 'gasolina restante'.$this->gasolina;
        }else {
            return 'esta seco hecha algo carburante si no no anda, excepto cuesta a bajo...';
        }
        
    }
}
